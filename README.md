# Base de Herramientas Impresas en 3D

Las herramientas pertenecen a cada autor respectivamente.

En cada carpeta podra localizar herramientas de un tipo con su archivo descargable y su direccion url al repositorio donde se descargo. 
Asi como una copia de la licencia usada por su autor original. 
Un pdf con las intruciones de fabricacion en español e ingles y una sub carpeta si procede con remix interesantes de la misma.
_______________________________________________________________________________________________________________________________________


The tools belong to each author respectively.

In each folder you can locate tools of a type with its downloadable file and its url to the repository where it was downloaded.
As well as a copy of the license used by its original author.
A pdf with the manufacturing instructions in Spanish and English and a sub-folder if necessary with interesting remixes of it.